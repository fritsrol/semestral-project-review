﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectModifier : MonoBehaviour
{
    /// <summary>
    /// In Ispector User chooses one option for object.
    /// Can see at the bottom in ChangeType enum.
    /// chosen option is aplied in phase 3
    /// </summary>
    public Color color = Color.white;
    public Texture2D texture;
    public Material material;
    public GameObject newObject;
    public Vector3 pos = new Vector3(0,0,0);
    public Vector3 rot = new Vector3(0,0,0);
    public Vector3 scal = new Vector3(1,1,1);
    private bool set = false;
    private GameObject thisGameObject;
    private MeshRenderer meshRenderer;

    [SerializeField]
    private ChangeType _selected = ChangeType.Color;
    // Start is called before the first frame update
    void Start()
    {
        thisGameObject = this.gameObject;
        meshRenderer = this.gameObject.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (RuteController.stage == 3 && !set)
        {
            set = true;
            switch (_selected)
            {
                case ChangeType.Color:
                    meshRenderer.material.color = color;
                    break;
                case ChangeType.Texture:
                    meshRenderer.material.mainTexture = texture;
                    break;
                case ChangeType.Material:
                    meshRenderer.material = material;
                    break;
                case ChangeType.Object:
                    thisGameObject.SetActive(false);
                    if (newObject != null)
                        newObject.SetActive(true);
                    break;
                case ChangeType.Transform:
                    thisGameObject.transform.position = pos;
                    thisGameObject.transform.rotation = Quaternion.Euler(rot);
                    thisGameObject.transform.localScale = scal;
                    break;
            }
        }
    }
    public ChangeType GetSelected()
    {
        return _selected;
    }
    public void SetSelected(int id)
    {
        switch (id)
        {
            case 0:
                _selected = ChangeType.Color;
                break;
            case 1:
                _selected = ChangeType.Texture;
                break;
            case 2:
                _selected = ChangeType.Material;
                break;
            case 3:
                _selected = ChangeType.Object;
                break;
            case 4:
                _selected = ChangeType.Transform;
                break;
        }
    }
}

public enum ChangeType : int
{
    NotSet = -1,
    Color = 0,
    Texture = 1,
    Material = 2,
    Object = 3,
    Transform = 4
}
