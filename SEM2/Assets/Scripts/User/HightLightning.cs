﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HightLightning : MonoBehaviour
{
    /// <summary>
    /// If object uses material with Fresnell Effect shader, then this turns on and off this effect.
    /// </summary>
    // Start is called before the first frame update
    private Material material;
    void Start()
    {
        material = this.gameObject.GetComponent<Renderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (RuteController.stage == 2)
        {
            material.SetInt("_hightlight", 1);
        }
        else if (RuteController.stage == 0 || RuteController.stage == 3)
        {
            material.SetInt("_hightlight", 0);
        }
    }
}
