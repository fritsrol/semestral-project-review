﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowScript : MonoBehaviour
{
    /// <summary>
    /// Shows object in one predefined stage
    /// </summary>
    
    // When true then object is visible only in targetStage
    // else its visibility changes when player reach finish in targetStage
    [SerializeField]
    private bool ChangeOnStage = true;
    [SerializeField]
    
    // if true then when player reaches finish in specified stage, object hides, else otherwise
    //only when ChangeOnStage is false it is used
    private bool hideAtTheStart = true;
    [SerializeField]
    private int targetStage = 2;

    //target object
    private GameObject plane;

    [SerializeField]
    private bool FaceToPlayer = false;
    private Camera cam = null;
    void Start()
    {
        plane = transform.GetChild(0).gameObject;
        if (FaceToPlayer)
        {
            cam = Camera.main;
            if (cam == null)
            {
                Debug.LogError("You have to set a main camera");
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        bool isActive = plane.activeSelf;
        if (RuteController.stage != targetStage && isActive)
        {
            plane.SetActive(false);
        }
        else if (ChangeOnStage && RuteController.stage == targetStage && !isActive)
        {
            plane.SetActive(true);
        }
        else if (RuteController.startReached && !ChangeOnStage)
        {
            if (hideAtTheStart && isActive)
            {
                plane.SetActive(false);
            }
            else if (!hideAtTheStart && !isActive)
            {
                plane.SetActive(true);
            }
        }
        else if (!ChangeOnStage && hideAtTheStart && RuteController.stage == targetStage)
        {
            plane.SetActive(true);
        }


            if (FaceToPlayer & isActive)
        {
            this.transform.LookAt(cam.transform);
        }
    }
}
