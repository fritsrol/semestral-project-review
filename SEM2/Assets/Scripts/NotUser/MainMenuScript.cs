﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuScript : MonoBehaviour
{
    /// <summary>
    /// Not used in semestral project edition
    /// </summary>
    [SerializeField]
    private Button startButton;
    [SerializeField]
    private Button quitButton;
    [SerializeField]
    private ScrollRect scrollRect;



    public void QuitAction()
    {
        Debug.Log("Quit app...");
    }

    public void PlayAction()
    {
        Debug.Log("Play app...");
        
    }
}
