﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NaivePortal : MonoBehaviour
{
    /// <summary>
    /// Lame portal edition :(
    /// sets naive portal
    /// </summary>
    [SerializeField]
    private GameObject playerObject;
    [SerializeField]
    Teleporter teleport;
    [SerializeField]
    Transform otherPortal;

    // Start is called before the first frame update
    void Start()
    {

        PlayerController player = playerObject.GetComponent<PlayerController>();

        teleport.SetCharacterController(playerObject.GetComponent<CharacterController>());
        teleport.SetPlayerController(player);
        teleport.SetReciever(otherPortal.GetChild(0));
    }
}
