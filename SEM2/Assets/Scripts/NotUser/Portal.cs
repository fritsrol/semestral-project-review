﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    /// <summary>
    /// Sets portal components
    /// </summary>
    [SerializeField]
    private GameObject playerObject;
    [SerializeField]
    Teleporter teleport;
    [SerializeField]
    PortalCamera portalCamera;
    [SerializeField]
    Transform otherPortal;
    private Material camMaterial;
    [SerializeField]
    Shader camShader;

    private GameObject renderPlane;
    // Start is called before the first frame update
    void Start()
    {
        camMaterial = new Material(camShader);

        PlayerController player = playerObject.GetComponent<PlayerController>();

        teleport.SetCharacterController(playerObject.GetComponent<CharacterController>());
        teleport.SetPlayerController(player);
        teleport.SetReciever(otherPortal.GetChild(2));

        portalCamera.SetPortal(this.transform);
        portalCamera.SetOtherPortal(otherPortal);

        if (RuteController.VR)
        {
            portalCamera.SetPlayerCamera(playerObject.transform.GetChild(3).GetChild(2).gameObject.GetComponent<Camera>());
        }
        else
            portalCamera.SetPlayerCamera(playerObject.transform.GetChild(2).gameObject.GetComponent<Camera>());

        MeshRenderer meshRenderer = otherPortal.GetChild(1).gameObject.GetComponent<MeshRenderer>();
        meshRenderer.material = camMaterial;
        portalCamera.SetCamMaterial(camMaterial);
        portalCamera.SetRenderPlane(meshRenderer);
        portalCamera.SetDefaults();

        renderPlane = this.transform.GetChild(1).gameObject;
        renderPlane.SetActive(false);
    }

    private void Update()
    {
        if (RuteController.stage == 3 && !renderPlane.activeSelf)
        {
            renderPlane.SetActive(true);
        }
    }
}
