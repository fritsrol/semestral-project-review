﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

//https://answers.unity.com/questions/192895/hideshow-properties-dynamically-in-inspector.html

[CustomEditor(typeof(ObjectModifier))]
public class ObjectModifierEditor : Editor
{
    /// <summary>
    /// Edits behavior of Modifier class in Inspector
    /// uses enum to set used variable.
    /// </summary>
    ObjectModifier myScript = null;
    string[] _options = new string[5] {ChangeType.Color.ToString(), ChangeType.Texture.ToString(), ChangeType.Material.ToString(), ChangeType.Object.ToString(), ChangeType.Transform.ToString() };
    override public void OnInspectorGUI()
     {
        if (myScript == null)
        myScript = target as ObjectModifier;

        myScript.SetSelected(EditorGUILayout.Popup("Choose change", (int)myScript.GetSelected(), _options));

        EditorGUI.indentLevel++;
        EditorGUILayout.PrefixLabel(myScript.GetSelected().ToString());
        switch(myScript.GetSelected())
        {
            case ChangeType.Color:
                myScript.color = EditorGUILayout.ColorField(myScript.color);
                break;
            case ChangeType.Texture:
                myScript.texture = (Texture2D)EditorGUILayout.ObjectField(myScript.texture, typeof(Texture2D), false, GUILayout.Width(70), GUILayout.Height(70));
                //https://answers.unity.com/questions/1424385/how-to-display-texture-field-with-label-on-top-lik.html
                break;
            case ChangeType.Material:
                myScript.material = (Material)EditorGUILayout.ObjectField(myScript.material, typeof(Material), false);
                break;
            case ChangeType.Object:
                myScript.newObject = (GameObject)EditorGUILayout.ObjectField(myScript.newObject, typeof(GameObject), false);
                break;
            case ChangeType.Transform:
                myScript.pos = EditorGUILayout.Vector3Field("Position", myScript.pos);
                myScript.rot = EditorGUILayout.Vector3Field("Rotation", myScript.rot);
                myScript.scal = EditorGUILayout.Vector3Field("Scale", myScript.scal);
                break;
        }
        EditorGUI.indentLevel--;

    }
}
