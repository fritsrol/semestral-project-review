﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    /// <summary>
    /// Fade in and out effect
    /// </summary>
    [SerializeField]
    private Image image = null;
    [SerializeField]
    private float fadeTime = 2.0f;

    [HideInInspector]
    public bool _fadeIn = false;
    [HideInInspector]
    public int actualStage = -1;
    // Start is called before the first frame update
    void Start()
    {
        image.canvasRenderer.SetAlpha(0f);
        //fadeIn();
        //fadeOut();
    }

    public void fadeIn()
    {
        if (image == null)
        {
            Debug.LogError("Fade image is null!");
            return;
        }
        image.CrossFadeAlpha(1.0f, fadeTime, false);
    }

    public void fadeOut()
    {
        if (image == null)
        {
            Debug.LogError("Fade image is null!");
            return;
        }
        image.CrossFadeAlpha(0.0f, fadeTime, false);
    }

    private void Update()
    {
        if (_fadeIn && image.canvasRenderer.GetAlpha() == 0.0f) {
            _fadeIn = false;
            fadeIn();
        }
        else if (image.canvasRenderer.GetAlpha() == 1.0f)
        {
            RuteController.stage = actualStage;
            fadeOut();
        }
    }
}
