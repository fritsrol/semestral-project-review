﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    /// <summary>
    /// Teleports player
    /// </summary>
    private PlayerController playerController;
    private CharacterController player;

    // Collider plane
    private Transform reciever; //second portal


    // Sends player between portals
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (RuteController.stage != 3)
            {
                return;
            }
            if (!playerController.PlayerIsTeleported)
            {
                Vector3 portalToPlayer = -(player.transform.position - transform.position);
                portalToPlayer.y *= -1;

                Vector3 rotationDiff = (-transform.rotation.eulerAngles + reciever.rotation.eulerAngles);

                player.transform.Rotate(rotationDiff + new Vector3(0f, 180f, 0f)); //up

                Vector3 possitionOffset = Quaternion.Euler(rotationDiff) * portalToPlayer;
                player.enabled = false;
                player.transform.position = reciever.position + possitionOffset;
                player.enabled = true;
                playerController.PlayerIsTeleported = true;
            }
            else
            {
                playerController.PlayerIsTeleported = false;
            }
        }
    }

    public void SetReciever (Transform colliderPlane)
    {
        reciever = colliderPlane;
    }

    public void SetPlayerController(PlayerController playerC)
    {
        playerController = playerC;
    }

    public void SetCharacterController(CharacterController characterController)
    {
        player = characterController;
    }
}
