﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MyRenderPlane
{
    /// <summary>
    /// source:
    /// https://www.youtube.com/watch?v=cWpFZbjtSQg
    /// 
    /// Controls if player camera sees portal render plane. If not
    /// there is no reason to render on portal.
    /// </summary>
    public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
    }
}
