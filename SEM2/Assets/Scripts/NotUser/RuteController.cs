﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Valve.VR;
using UnityEngine.SceneManagement;

public class RuteController : MonoBehaviour
{
    /// <summary>
    /// Controls simulation
    /// </summary>
    public bool skip = true;
    [SerializeField]
    private bool vr = false;
    public static bool VR = false;
    [SerializeField]
    private GameObject playerObject;
    private PlayerController player;
    private NavMeshAgent agent;

    [SerializeField]
    private SteamVR_Input_Sources handType;
    [SerializeField]
    private SteamVR_Action_Boolean startStage;
    [SerializeField]
    private SteamVR_Action_Boolean backToMenu;

    [SerializeField]
    private Transform[] points;
    private int point = 0;
    public static int stage = 0;
    [SerializeField]
    private float radius = 2; // in phase 1 user has to come back to start. This is radius of start around the start point
    private Vector3 start;
    public static bool startReached = false;

    [SerializeField]
    private Fade fade;

    private void Awake()
    {
        VR = vr;
    }
    // Start is called before the first frame update
    void Start()
    {
        player = playerObject.GetComponent<PlayerController>();
        agent = playerObject.GetComponent<NavMeshAgent>();

        player.SetCanWalk(false);
        start = playerObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (backToMenu.GetStateDown(handType))
        {
            SceneManager.LoadScene(0); //load menu scene (not in semestral project edition)
            return;
        }
        if (stage == 3)
        {
            return;
        }

        if (skip)
        {
            stage = 3;
            player.SetPosition(points[points.Length - 1].position);
            player.SetCanWalk(true);
            return;
        }

        // Starts stage 1
        if (stage == 0 && ((Input.GetKeyDown(KeyCode.Space) && !VR) || (VR && startStage.GetStateDown(handType))))
        {
            agent.SetDestination(points[point].position);
            stage++; // 1
        }
        else if (stage == 0)
        {
            return;
        }

        // moving via rute in stage 1
        if (Vector3.Distance(playerObject.transform.position, points[point].position) < 1f && stage == 1)
        {
            if (point == points.Length - 1) // finish
            {
                //stage = 2
                fade.actualStage = 2;
                fade._fadeIn = true;
                player.SetCanWalk(true);
            }
            else
            {
                ++point;
                agent.SetDestination(points[point].position);
            }
        }
        else if (stage == 2 && Vector3.Distance(playerObject.transform.position, start) <= radius) // user reached start point in stage 2
        {
            startReached = true;
        }
        // start stage 3
        if (startReached && stage == 2 && ((Input.GetKeyDown(KeyCode.Space) && !VR) || (VR && startStage.GetStateDown(handType))))
        {

            //stage = 3
            fade.actualStage = 3;
            fade._fadeIn = true;
            player.SetPosition(points[point].position);
        }
    }
}
