﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class PortalCamera : MonoBehaviour
{
    /// <summary>
    /// Not used in semestral project edition
    /// 
    /// tons of inspiration:
    /// https://www.youtube.com/watch?v=cWpFZbjtSQg
    /// - Coding adventure portal
    /// </summary>
    private Camera playerCamera;
    private Transform portal;
    private Transform otherPortal;
    private MeshRenderer renderPlane;
    [SerializeField]
    private float offsetForClipPlane = 0.2f; // sometimes there is edge around portal, this should fix it
                                             // But it would make mistakes in more detailed areas
    [SerializeField]
    private float nearClipLimit = 0.2f;

    private Camera portalCamera;

    private Material cameraMat;
    private RenderTexture renderTexture;

    private bool seenBefore = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (RuteController.stage != 3) return; // no need to use before stage 3

        if(!MyRenderPlane.IsVisibleFrom(renderPlane, playerCamera))
        {
            if (seenBefore) // turns of rendering, when player does not see portal
            {
                Texture2D texture = new Texture2D(1, 1);
                texture.SetPixel(0, 0, Color.magenta);
                texture.Apply();
                cameraMat.mainTexture = texture;
                seenBefore = false;
            }
            return;
        }
        if (!seenBefore)
        {
            cameraMat.mainTexture = renderTexture;
            seenBefore = true;
        }

        Vector3 playerOffsetFromPortal = -(playerCamera.transform.position - otherPortal.position);
        playerOffsetFromPortal.y *= -1;

        Quaternion portalRotDiff = Quaternion.Euler(portal.rotation.eulerAngles - otherPortal.rotation.eulerAngles);


        Vector3 newPosition = portal.position + portalRotDiff * playerOffsetFromPortal;
        transform.position = newPosition;

        Quaternion newRotation = Quaternion.Euler(playerCamera.transform.rotation.eulerAngles + portal.rotation.eulerAngles - otherPortal.rotation.eulerAngles + new Vector3(0f, 180f, 0f));
        transform.rotation = newRotation;

        SetNearClipPlane();
    }

    // from coding adventure portal
    private void SetNearClipPlane()
    {
        Transform clipPlane = portal;
        float dot = Mathf.Sign(Vector3.Dot(clipPlane.forward, clipPlane.position - transform.position));
        Vector3 camSpacePos = portalCamera.worldToCameraMatrix.MultiplyPoint(clipPlane.position);
        Vector3 camSpaceNormal = portalCamera.worldToCameraMatrix.MultiplyVector(clipPlane.forward) * dot;
        float camSpaceDst = -Vector3.Dot(camSpacePos, camSpaceNormal) +offsetForClipPlane;

        if (Mathf.Abs(camSpaceDst) > nearClipLimit)
        {
            Vector4 clipPlaneCamSpace = new Vector4(camSpaceNormal.x, camSpaceNormal.y, camSpaceNormal.z, camSpaceDst);

            portalCamera.projectionMatrix = playerCamera.CalculateObliqueMatrix(clipPlaneCamSpace);
        }
        else
        {
            portalCamera.projectionMatrix = playerCamera.projectionMatrix;
        }
    }

    //from coding adventure portal, I may use it sometimes

    // Sets the thickness of the portal screen so as not to clip with camera near plane when player goes through
    /*float ProtectScreenFromClipping(Vector3 viewPoint)
    {
        float halfHeight = playerCamera.nearClipPlane * Mathf.Tan(playerCamera.fieldOfView * 0.5f * Mathf.Deg2Rad);
        float halfWidth = halfHeight * playerCamera.aspect;
        float dstToNearClipPlaneCorner = new Vector3(halfWidth, halfHeight, playerCamera.nearClipPlane).magnitude;
        float screenThickness = dstToNearClipPlaneCorner;

        Transform screenT = otherPortal.GetChild(1).transform;
        bool camFacingSameDirAsPortal = Vector3.Dot(transform.forward, transform.position - viewPoint) > 0;
        screenT.localScale = new Vector3(screenT.localScale.x, screenT.localScale.y, screenThickness);
        screenT.localPosition = Vector3.forward * screenThickness * ((camFacingSameDirAsPortal) ? 0.5f : -0.5f);
        return screenThickness;
    }*/

    public void SetPlayerCamera(Camera cam)
    {
        playerCamera = cam;
    }

    public void SetPortal(Transform portal)
    {
        this.portal = portal;
    }

    public void SetOtherPortal(Transform portal)
    {
        otherPortal = portal;
    }

    public void SetRenderPlane(MeshRenderer meshRenderer)
    {
        renderPlane = meshRenderer;
    }

    public void SetCamMaterial(Material material)
    {
        cameraMat = material;
    }

    public void SetDefaults()
    {
        portalCamera = this.GetComponent<Camera>();

        if (portalCamera.targetTexture != null)
        {
            portalCamera.targetTexture.Release();
        }
        portalCamera.targetTexture = new RenderTexture(Screen.width, Screen.height, 32, RenderTextureFormat.ARGB32);
        renderTexture = portalCamera.targetTexture;
        cameraMat.mainTexture = renderTexture;
    }
}
