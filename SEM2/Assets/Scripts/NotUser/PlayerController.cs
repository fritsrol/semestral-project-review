﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class PlayerController: MonoBehaviour
{
    /// <summary>
    /// source:
    /// https://www.youtube.com/watch?v=_QajrabyTJc
    /// </summary>

    [HideInInspector]
    public bool PlayerIsTeleported = false;

    [SerializeField]
    private SteamVR_Action_Vector2 moveAxis;
    [SerializeField]
    private SteamVR_Action_Pose pose;


    [SerializeField]
    private CharacterController controller;
    [SerializeField]
    private float speed = 1f;
    private float gravity = 9.81f;

    [SerializeField]
    private Transform groundCheck;
    [SerializeField]
    private float groundDistance = 0.2f;
    [SerializeField]
    private LayerMask groundMask;
    private Vector3 velocity;
    private bool isGrounded;
    private bool canWalk = true;

    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (canWalk)
        {

            float x = 0;
            float z = 0;
            Vector3 move;

            if (RuteController.VR)
            {
                if (moveAxis.axis.magnitude > 0.1f)
                {
                    x = -moveAxis.axis.x;
                    z = -moveAxis.axis.y;
                }
                move = pose.localRotation * new Vector3(x, 0, z);
            }
            else
            {
                x = Input.GetAxis("Horizontal");
                z = Input.GetAxis("Vertical");
                move = x * transform.right + z * transform.forward;
            }
            controller.Move(move * Time.deltaTime * speed);

            isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

            if (isGrounded && velocity.y < 0)
            {
                velocity.y = -2f;
            }
            velocity.y -= gravity * Time.deltaTime;
            controller.Move(velocity * Time.deltaTime);
        }
    }

    public void SetCanWalk(bool can)
    {
        canWalk = can;
    }

    public bool GetCanWalk()
    {
        return canWalk;
    }

    public void SetPosition(Vector3 position)
    {
        canWalk = false;
        controller.enabled = false;
        transform.position = position;
        controller.enabled = true;
        canWalk = true;
    }
}
