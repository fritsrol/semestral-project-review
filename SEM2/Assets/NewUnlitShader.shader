﻿Shader "Unlit/NewUnlitShader"
{
	Properties
	{
		[MainTexture] _BaseMap("Texture", 2D) = "white" {}
		[MainColor]   _BaseColor("Color", Color) = (1, 1, 1, 1)
		_Cutoff("AlphaCutout", Range(0.0, 1.0)) = 0.5

			// BlendMode
			[HideInInspector] _Surface("__surface", Float) = 0.0
			[HideInInspector] _Blend("__blend", Float) = 0.0
			[HideInInspector] _AlphaClip("__clip", Float) = 0.0
			[HideInInspector] _SrcBlend("Src", Float) = 1.0
			[HideInInspector] _DstBlend("Dst", Float) = 0.0
			[HideInInspector] _ZWrite("ZWrite", Float) = 1.0
			[HideInInspector] _Cull("__cull", Float) = 2.0

			// Editmode props
			[HideInInspector] _QueueOffset("Queue offset", Float) = 0.0

			// ObsoleteProperties
			[HideInInspector] _MainTex("BaseMap", 2D) = "white" {}
			[HideInInspector] _Color("Base Color", Color) = (0.5, 0.5, 0.5, 1)
			[HideInInspector] _SampleGI("SampleGI", float) = 0.0 // needed from bakedlit
	}
		SubShader
			{
				Tags { "RenderType" = "Opaque" "IgnoreProjector" = "True" "RenderPipeline" = "UniversalPipeline" }
				LOD 100

				Blend[_SrcBlend][_DstBlend]
				ZWrite[_ZWrite]
				Cull[_Cull]

				Pass
				{
					Name "Unlit"
					HLSLPROGRAM
				// Required to compile gles 2.0 with standard srp library
				#pragma prefer_hlslcc gles
				#pragma exclude_renderers d3d11_9x

				#pragma vertex vert
				#pragma fragment frag
				#pragma shader_feature _ALPHATEST_ON
				#pragma shader_feature _ALPHAPREMULTIPLY_ON

				// -------------------------------------
				// Unity defined keywords
				#pragma multi_compile_fog
				#pragma multi_compile_instancing

				#include "Packages/com.unity.render-pipelines.universal/Shaders/UnlitInput.hlsl"

				struct Attributes
				{
					float4 positionOS       : POSITION;
					float2 uv               : TEXCOORD0;
					UNITY_VERTEX_INPUT_INSTANCE_ID
				};

				struct Varyings
				{
					float2 uv        : TEXCOORD0;
					float fogCoord : TEXCOORD1;
					float4 screenPos : TEXCOORD2;
					float4 vertex : SV_POSITION;

					UNITY_VERTEX_INPUT_INSTANCE_ID
					UNITY_VERTEX_OUTPUT_STEREO
				};

				//https://github.com/TwoTailsGames/Unity-Built-in-Shaders/blob/master/CGIncludes/UnityCG.cginc
				inline float4 ComputeNonStereoScreenPos(float4 pos) {
					float4 o = pos * 0.5f;
					o.xy = float2(o.x, o.y * _ProjectionParams.x) + o.w;
					o.zw = pos.zw;
					return o;
				}

				Varyings vert(Attributes input)
				{
					Varyings output = (Varyings)0;

					UNITY_SETUP_INSTANCE_ID(input);
					UNITY_TRANSFER_INSTANCE_ID(input, output);
					UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

					VertexPositionInputs vertexInput = GetVertexPositionInputs(input.positionOS.xyz);
					output.vertex = vertexInput.positionCS;
					output.screenPos = ComputeNonStereoScreenPos(output.vertex);
					//output.screenPos = ComputeScreenPos(output.vertex);
					output.uv = TRANSFORM_TEX(input.uv, _BaseMap);
					output.fogCoord = ComputeFogFactor(vertexInput.positionCS.z);

					return output;
				}

				half4 frag(Varyings input) : SV_Target
				{
					UNITY_SETUP_INSTANCE_ID(input);
					UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
					float4 scaleOffset = unity_StereoScaleOffset[unity_StereoEyeIndex];
					
					//input.screenPos = scaleOffset;
					input.screenPos /= input.screenPos.w;
					//input.screenPos.x -= 0.03;
					if (unity_StereoEyeIndex == 0)
					{
						//input.screenPos.x -= 0.03;
					}
					else
					{
						//input.screenPos.x += 0.03;
					}


					half2 uv = float2(input.screenPos.x, input.screenPos.y); //input.uv;
					//https://docs.unity3d.com/Manual/SinglePassStereoRendering.html?_ga=2.129870796.1786816548.1610028782-1772376704.1605892532
					//uv = uv.xy * scaleOffset.xy +scaleOffset.zw * input.screenPos.w; //input.screenPos.w
					
					// If Single-Pass Stereo mode is active, transform the
					// coordinates to get the correct output UV for the current eye.
						//float4 scaleOffset = unity_StereoScaleOffset[unity_StereoEyeIndex];
						//uv = (uv - scaleOffset.xy) / scaleOffset.zw;

					half4 texColor = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, uv);

					half3 color = texColor.rgb * _BaseColor.rgb;
					half alpha = texColor.a * _BaseColor.a;
					AlphaDiscard(alpha, _Cutoff);

	#ifdef _ALPHAPREMULTIPLY_ON
					color *= alpha;
	#endif

					color = MixFog(color, input.fogCoord);
					alpha = OutputAlpha(alpha);

					return half4(color, alpha);
				}
				ENDHLSL
			}
			Pass
			{
				Tags{"LightMode" = "DepthOnly"}

				ZWrite On
				ColorMask 0

				HLSLPROGRAM
					// Required to compile gles 2.0 with standard srp library
					#pragma prefer_hlslcc gles
					#pragma exclude_renderers d3d11_9x
					#pragma target 2.0

					#pragma vertex DepthOnlyVertex
					#pragma fragment DepthOnlyFragment

					// -------------------------------------
					// Material Keywords
					#pragma shader_feature _ALPHATEST_ON

					//--------------------------------------
					// GPU Instancing
					#pragma multi_compile_instancing

					#include "Packages/com.unity.render-pipelines.universal/Shaders/UnlitInput.hlsl"
					#include "Packages/com.unity.render-pipelines.universal/Shaders/DepthOnlyPass.hlsl"
					ENDHLSL
				}

					// This pass it not used during regular rendering, only for lightmap baking.
					Pass
					{
						Name "Meta"
						Tags{"LightMode" = "Meta"}

						Cull Off

						HLSLPROGRAM
					// Required to compile gles 2.0 with standard srp library
					#pragma prefer_hlslcc gles
					#pragma exclude_renderers d3d11_9x
					#pragma vertex UniversalVertexMeta
					#pragma fragment UniversalFragmentMetaUnlit

					#include "Packages/com.unity.render-pipelines.universal/Shaders/UnlitInput.hlsl"
					#include "Packages/com.unity.render-pipelines.universal/Shaders/UnlitMetaPass.hlsl"

					ENDHLSL
				}
			}
				FallBack "Hidden/Universal Render Pipeline/FallbackError"
					CustomEditor "UnityEditor.Rendering.Universal.ShaderGUI.UnlitShader"
}
