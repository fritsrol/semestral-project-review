﻿Shader "Custom/CameraCutOffShader"
{
	//Mnoho ruznych neuspesnych pokusu
	// source:
	//https://www.youtube.com/watch?v=cuQao3hEKfs
		Properties
		{
			_MainTex("Texture", 2D) = "white" {}
		}
			SubShader
		{
			Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
			Lighting Off
			Cull Back //off
			ZWrite On
			ZTest Less

			Fog{ Mode Off }

			Pass
			{
				CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag

				#include "UnityCG.cginc"

				struct appdata
				{
					float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
					//UNITY_INSTANCE_ID
				};

				struct v2f
				{
					//float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
					float4 screenPos : TEXCOORD1;
					//UNITY_INSTANCE_ID
					//UNITY_VERTEX_OUTPUT_STEREO
				};

				v2f vert(appdata v)
				{
					v2f o;
					//UNITY_SETUP_INSTANCE_ID(v)
					//UNITY_TRANSFER_INSTANCE_ID(v, o)
					
					//UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o)
					//o.uv = v.uv;
					o.vertex = UnityObjectToClipPos(v.vertex);
					o.screenPos = ComputeScreenPos(o.vertex);
					return o;
				}

				sampler2D _MainTex;
				//UNITY_DECLARE_SCREENSPACE_TEXTURE(_MainTex);

				fixed4 frag(v2f i) : SV_Target
				{
					//UNITY_SETUP_INSTANCE_ID(i);
					//fixed4 col = UNITY_SAMPLE_SCREENSPACE_TEXTURE(_MainTex, i.uv);
					// just invert the colors
					//col = 1 - col;
					i.screenPos /= i.screenPos.w;
				i.screenPos.x -= 0.03;
				if (unity_StereoEyeIndex == 0)
				{
					i.screenPos.x -= 0.03;
				}
				else
				{
					i.screenPos.x += 0.03;
				}
					fixed4 col = tex2D(_MainTex, float2(i.screenPos.x, i.screenPos.y));

					return col;
				}
				ENDCG
			}
		}
}
